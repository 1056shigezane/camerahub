//
//  CameraHubApp.swift
//  CameraHub
//
//  Created by Narumi Inada on 2022/08/09.
//

import SwiftUI
import SystemExtensions
import CoreMediaIO
import Combine
import Aether

@main
struct CameraHubApp: App {
    
    //@Published var foundServices: [Any] = []
    
    var cancellables: Set<AnyCancellable> = .init()
    
    
    init(){
        ServerDirectory.shared.foundServices
    }
    
    class RequestDelegate: NSObject, OSSystemExtensionRequestDelegate, ObservableObject  {
        
        @Published var logText: String = ""
        
        func request(_ request: OSSystemExtensionRequest, actionForReplacingExtension existing: OSSystemExtensionProperties, withExtension ext: OSSystemExtensionProperties) -> OSSystemExtensionRequest.ReplacementAction {
            logText = "actionForReplacingExtension" + existing.bundleShortVersion
            
            
            
            
            return .replace
        }
        
        func requestNeedsUserApproval(_ request: OSSystemExtensionRequest) {
            logText = "NeeedsUserApproval"
        }
        
        func request(_ request: OSSystemExtensionRequest, didFinishWithResult result: OSSystemExtensionRequest.Result) {
            logText = "didFinishWithResult" + "\(result.rawValue)"
        }
        
        func request(_ request: OSSystemExtensionRequest, didFailWithError error: Error) {
            logText = error.localizedDescription
        }
        
        
    }
    @StateObject var requestDelegate = RequestDelegate.init()
    let extensionsDirectoryURL = URL(fileURLWithPath: "Contents/Library/SystemExtensions", relativeTo: Bundle.main.bundleURL)
    
    var _extensionBundle: Bundle {
        let extensionURLs: [URL]
        do{
            extensionURLs = try FileManager.default.contentsOfDirectory(at: extensionsDirectoryURL, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
            
        }catch{
            fatalError(error.localizedDescription)
        }
        
        guard let extensionURL = extensionURLs.first else{ fatalError("ExtensionURL not found") }
        guard let extensionBundle = Bundle(url: extensionURL) else{ fatalError("extension Bundle Not Found.") }
        return extensionBundle
    }
    
    var body: some Scene {
        WindowGroup {
            
            VStack{
                HStack{
                    Button("Install") {
                        guard let id = _extensionBundle.bundleIdentifier else{ print("fail"); return }
                        let activationRequest = OSSystemExtensionRequest.activationRequest(forExtensionWithIdentifier: id, queue: .main)
                        activationRequest.delegate = requestDelegate
                        
                        
                        DispatchQueue.global().async {
                            OSSystemExtensionManager.shared.submitRequest(activationRequest)
                            
//                            CMIOExtensionProvider.startService(provider: providerSource)
                        }
                    }
                    
                    Button("Uninstall") {
                        guard let id = _extensionBundle.bundleIdentifier else{ print("fail"); return }
                        let deactivationRequest = OSSystemExtensionRequest.deactivationRequest(forExtensionWithIdentifier: id, queue: .global())
                        deactivationRequest.delegate = requestDelegate
                        OSSystemExtensionManager.shared.submitRequest(deactivationRequest)
                        
                        
                    }
                }
                
                
                Text(requestDelegate.logText)
                
                
                
                
            }.onAppear{
                
            }
            
            
            
        }
    }
}




