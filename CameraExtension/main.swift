//
//  main.swift
//  CameraExtension
//
//  Created by Narumi Inada on 2022/08/09.
//

import Foundation
import CoreMediaIO

let providerSource = CameraExtensionProviderSource(clientQueue: nil)
CMIOExtensionProvider.startService(provider: providerSource.provider)

CFRunLoopRun()
