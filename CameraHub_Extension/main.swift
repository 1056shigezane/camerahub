//
//  main.swift
//  CameraHub_Extension
//
//  Created by Narumi Inada on 2022/08/10.
//

import Foundation
import CoreMediaIO


import Aether

ServerDirectory.shared.foundServices



let providerSource = CameraHub_ExtensionProviderSource(clientQueue: nil)
CMIOExtensionProvider.startService(provider: providerSource.provider)

CFRunLoopRun()
